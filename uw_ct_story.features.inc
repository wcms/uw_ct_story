<?php

/**
 * @file
 * uw_ct_story.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_story_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_story_node_info() {
  $items = array(
    'uw_stories' => array(
      'name' => t('Story'),
      'base' => 'node_content',
      'description' => t('Stories about the University of Waterloo.'),
      'has_title' => '1',
      'title_label' => t('Headline'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function uw_ct_story_paragraphs_info() {
  $items = array(
    'uw_stories_block' => array(
      'name' => 'Story block',
      'bundle' => 'uw_stories_block',
      'locked' => '1',
    ),
  );
  return $items;
}

/**
 * Implements hook_rdf_default_mappings().
 */
function uw_ct_story_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: uw_stories.
  $schemaorg['node']['uw_stories'] = array(
    'rdftype' => array(
      0 => 'schema:Article',
      1 => 'sioc:Item',
      2 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'schema:name',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'schema:articleBody',
        1 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'schema:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'field_story_preview_image' => array(
      'predicates' => array(
        0 => 'schema:thumbnail',
      ),
      'type' => 'rel',
    ),
    'url' => array(
      'predicates' => array(
        0 => 'schema:url',
      ),
      'type' => 'rel',
    ),
    'field_story_author_role' => array(
      'predicates' => array(
        0 => 'schema:author',
      ),
    ),
    'field_story_author_name' => array(
      'predicates' => array(
        0 => 'schema:author',
      ),
    ),
    'field_story_subhead' => array(
      'predicates' => array(
        0 => 'schema:subhead',
      ),
    ),
    'field_story_summary' => array(
      'predicates' => array(),
    ),
    'field_story_body' => array(
      'predicates' => array(
        0 => 'schema:articleBody',
      ),
    ),
    'field_story_societal_relevance' => array(
      'predicates' => array(
        0 => 'schema:articleSection',
      ),
      'type' => 'rel',
    ),
    'field_topics_area' => array(
      'predicates' => array(
        0 => 'schema:articleSection',
      ),
      'type' => 'rel',
    ),
    'field_topics_achievement' => array(
      'predicates' => array(
        0 => 'schema:articleSection',
      ),
      'type' => 'rel',
    ),
    'field_topics_research' => array(
      'predicates' => array(
        0 => 'schema:articleSection',
      ),
      'type' => 'rel',
    ),
    'field_story_topics_area' => array(
      'predicates' => array(
        0 => 'schema:articleSection',
      ),
      'type' => 'rel',
    ),
    'field_story_topics_achievement' => array(
      'predicates' => array(
        0 => 'schema:articleSection',
      ),
      'type' => 'rel',
    ),
    'field_story_topics_research' => array(
      'predicates' => array(
        0 => 'schema:articleSection',
      ),
      'type' => 'rel',
    ),
    'field_story_blocks' => array(
      'predicates' => array(
        0 => 'schema:articleBody',
      ),
    ),
    'field_story_related_stories' => array(
      'predicates' => array(),
    ),
    'field_story_external_links' => array(
      'predicates' => array(),
    ),
    'field_story_image' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_image' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_file' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
  );

  // Exported RDF mapping: uw_stories_block.
  $schemaorg['paragraphs_item']['uw_stories_block'] = array(
    'field_story_block_title' => array(
      'predicates' => array(
        0 => 'schema:articleBody',
      ),
    ),
    'url' => array(
      'predicates' => array(
        0 => 'schema:url',
      ),
      'type' => 'rel',
    ),
    'field_story_display_title' => array(
      'predicates' => array(),
    ),
    'field_story_copy' => array(
      'predicates' => array(),
    ),
  );

  return $schemaorg;
}
