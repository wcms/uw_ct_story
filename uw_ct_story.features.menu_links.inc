<?php

/**
 * @file
 * uw_ct_story.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_story_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies_areas:admin/structure/taxonomy/uw_stories_areas.
  $menu_links['menu-site-manager-vocabularies_areas:admin/structure/taxonomy/uw_stories_areas'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_stories_areas',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Areas',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_areas:admin/structure/taxonomy/uw_stories_areas',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_research:admin/structure/taxonomy/uw_stories_research.
  $menu_links['menu-site-manager-vocabularies_research:admin/structure/taxonomy/uw_stories_research'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_stories_research',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Research',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_research:admin/structure/taxonomy/uw_stories_research',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_societal-relevance:admin/structure/taxonomy/uw_stories_societal_relevance.
  $menu_links['menu-site-manager-vocabularies_societal-relevance:admin/structure/taxonomy/uw_stories_societal_relevance'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_stories_societal_relevance',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Societal Relevance',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_societal-relevance:admin/structure/taxonomy/uw_stories_societal_relevance',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_what-we-do:admin/structure/taxonomy/uw_stories_achievements.
  $menu_links['menu-site-manager-vocabularies_what-we-do:admin/structure/taxonomy/uw_stories_achievements'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_stories_achievements',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'What We Do',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_what-we-do:admin/structure/taxonomy/uw_stories_achievements',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Areas');
  t('Research');
  t('Societal Relevance');
  t('What We Do');

  return $menu_links;
}
