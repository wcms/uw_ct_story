<?php

/**
 * @file
 * uw_ct_story.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_story_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_stories content'.
  $permissions['create uw_stories content'] = array(
    'name' => 'create uw_stories content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_stories content'.
  $permissions['delete any uw_stories content'] = array(
    'name' => 'delete any uw_stories content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_stories content'.
  $permissions['delete own uw_stories content'] = array(
    'name' => 'delete own uw_stories content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_stories content'.
  $permissions['edit any uw_stories content'] = array(
    'name' => 'edit any uw_stories content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_stories content'.
  $permissions['edit own uw_stories content'] = array(
    'name' => 'edit own uw_stories content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_stories revision log entry'.
  $permissions['enter uw_stories revision log entry'] = array(
    'name' => 'enter uw_stories revision log entry',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_stories authored by option'.
  $permissions['override uw_stories authored by option'] = array(
    'name' => 'override uw_stories authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_stories authored on option'.
  $permissions['override uw_stories authored on option'] = array(
    'name' => 'override uw_stories authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_stories promote to front page option'.
  $permissions['override uw_stories promote to front page option'] = array(
    'name' => 'override uw_stories promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_stories published option'.
  $permissions['override uw_stories published option'] = array(
    'name' => 'override uw_stories published option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_stories revision option'.
  $permissions['override uw_stories revision option'] = array(
    'name' => 'override uw_stories revision option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_stories sticky option'.
  $permissions['override uw_stories sticky option'] = array(
    'name' => 'override uw_stories sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search uw_stories content'.
  $permissions['search uw_stories content'] = array(
    'name' => 'search uw_stories content',
    'roles' => array(),
    'module' => 'search_config',
  );

  return $permissions;
}
